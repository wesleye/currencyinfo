const utils = require('./utils');

module.exports = class CurrencyPageParser {

    constructor(currencyCrawler) {
        this.currencyCrawler = currencyCrawler;
    }

    /**
     * Parses a currency page.
     * 
     * @param {Object} $ The crawled page
     * @param {string} iso ISO code of currency when type is currency
     * @param {string} country Country name of the parsed currency
     */
    parseCurrencyPage($, iso, country) {
        var rows = $('.infobox').find('tbody').find('tr');
        console.log(`Parsing ${iso}`);

        /**
         * Some currencies make a distiction between frequently
         * and rarely used banknotes / coins. If there is a distiction,
         * we need to parse the header and the following rows untill we find
         * the next known header (Coins).
         * 
         * Banknotes
         *      Freq. used          5, 10, 20
         *      Rarely used         50, 100
         * Coins
         *      Freq. used          0.10, 0.20, 0.50
         *      Rarely used         0.01, 0.02
         */        
        var currentHeader = null;
        rows.each((i, row) => {
            var e = $(row);
            const origText = e.find('td').text();
            const tdText = utils.trimText(origText);
    
            var tableHeader = e.find('th');
            var headerText = $(tableHeader).text();

            // If the currency has no discinction, just parse the td.
            if(!this.hasFrequencyDistiction(tdText) && (headerText === 'Banknotes' || headerText === 'Coins')) {
                // Just parse the header
                console.log(`Parsing single ${headerText}: ${tdText}`);
                return;
            }
    
/*
            if(headerText === 'Banknotes') {
                currentHeader = 'banknotes';
                console.log('Banknotes!');
            } else if(headerText === 'Coins') {
                currentHeader = 'coins';
                console.log('Coins!');
            } else if(headerText === ' Freq. used') {
                console.log(`Frequently used ${currentHeader}`);
            } else if(headerText === ' Rarely used') {
                console.log(`Rarely used ${currentHeader}`);
            }*/
        });
    }

    hasFrequencyDistiction(tdText) {
        if(tdText != null && tdText != '') {
            return false;
        }
        return true;
    }
}