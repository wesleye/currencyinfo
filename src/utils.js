const crypto = require('crypto');

/**
 * Trim a label from newlines and [] annotations
 *
 * @param {*} text
 */
exports.trimText = (text) => {
    return text.replace(/[\n\t\r]/g,"").trim().replace(/(\[.*\])/g,"");
}


/**
 * Get the sha hash of th eurl.
 */
exports.getFileHash = (url) => {
    return crypto.createHash('sha256').update(url).digest('hex');
}

/**
 * Parse and validate the url
 *
 * @param {*} url
 * @returns
 */
exports.parseUrl = (url) => {
    var base = 'https://en.wikipedia.org';
    if(url === undefined) {
        return false;
    }

    if(url.charAt(0) === '#') {
        return false;
    }

    return `${base}/${url}`;
}

/**
 * Create an empty currency object
 */
exports.createBaseCurrencyObject = () => {
    return {
        'name': null,
        'symbols': [],
        'iso': null,
        'countries': [],
        'fractionalUnit': null,
        'numberToBasic': null,
        'banknotes': {
            'rarely': [],
            'frequently': []
        },
        'coins': {
            'rarely': [],
            'frequently': []
        }
    };
}