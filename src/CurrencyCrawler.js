const Crawler = require('crawler');
const cheerio = require('cheerio');
const path = require('path');
const fs = require('fs');

const utils = require('./utils');
const BasePageParser = require('./BasePageParser');
const CurrencyPageParser = require('./CurrencyPageParser');

module.exports = class CurrencyCrawler {
    
    constructor(debug = false, maxDebugQueries = 5, useDebugCache = false) {
        this.debug = debug;
        this.debugQueriesLeft = maxDebugQueries;
        this.useDebugCache = useDebugCache;

        this.urlsCrawled = [];
        this.drainEventActive = false;
        this.crawler = null;
        this.basePageParser = null;

        this.baseUrl = 'https://en.wikipedia.org/wiki/List_of_circulating_currencies';
        this.cachePath = path.join(__dirname, '..', 'cache');

    }

    /**
     * Kick's off the crawling by adding the base url
     * to the queue.
     */
    start() {
        // Create crawler
        this.crawler = new Crawler({
            maxConnections : 1,
            rateLimit: 1000, // 1 every second
            callback : (error, res, done) => {
                this.handleCrawlerResult(error, res, done);
            }
        });

        this.crawler.on('drain', () => {
            this.drained();
        });

        this.basePageParser = new BasePageParser(this);
        this.currencyPageParser = new CurrencyPageParser(this);

        // Kick off the crawling
        this.crawlPage(this.baseUrl, 'base');
    }

    /**
     * Handles the results from the crawler.
     * 
     * @param {Object|null} error Error
     * @param {Object} res Result
     * @param {*} done ?
     */
    handleCrawlerResult(error, res, done) {
        if(error) {
            console.log(error);
            done();
            throw "Error during crawling";
        }

        const url =  'https://' + res.connection._host + res.req.path;

        if(this.useDebugCache) {
            const $ = this.handleCacheFileForUrl(url);
            if($ === null) {
                const filePath = this.getCacheFilePathForUrl(url);
                const cacheObject = {
                    body: res.body,
                    iso: res.options.iso,
                    country: res.options.country,
                    url: url,
                    type: res.options.type
                };
                fs.writeFileSync(filePath, JSON.stringify(cacheObject));
            }
        }
        
        if(res.options.type === 'base') {
            this.parseBasePage(res.$);
        } else if(res.options.type === 'currency') {
            this.parseCurrencyPage(res.$, res.options.iso, res.options.country);
        }
        done();
    }

    /**
     * Parses the base page for currency url's and adds them
     * to the queue.
     * 
     * @param {Object} $ The crawled page
     */
    parseBasePage($) {
        const resultObj = this.basePageParser.parseBasePage($);
        this.drainEventActive = true;
    }

    /**
     * Parses a currency page.
     * 
     * @param {Object} $ The crawled page
     * @param {string} iso ISO code of currency when type is currency
     * @param {string} country Country name of the parsed currency
     */
    parseCurrencyPage($, iso, country) {
        const resultObj = this.currencyPageParser.parseCurrencyPage($, iso, country);
        console.log(resultObj);
    }

    /**
     * Called when the queue has drained. Ignores the first time
     * as it is also drained when just the base page has finished.
     */
    drained() {
        if(!this.drainEventActive) {
            return;
        }

        console.log('Drained!');
    }

    /**
     * Adds a new url to the queue.
     * 
     * @param {string} url Url to parse
     * @param {string} type Page type (base|currency)
     * @param {string} iso ISO code of currency when type is currency
     * @param {string} country Country name of the parsed currency
     */
    crawlPage(url, type, iso = null, country = null) {

        this.urlsCrawled.push(url);

        // Return cached file if active
        if(this.useDebugCache && this.handleCacheFileForUrl(url) !== null) {
            return;
        }
        
        // Otherwise, add it to the crawler
        this.crawler.queue({
            uri: url,
            type: type,
            iso: iso,
            country: country
        });
    }

    /**
     * Get the full path to the cache file
     * 
     * @param {String} url Url of the request
     */
    getCacheFilePathForUrl(url) {
        const fileHash = utils.getFileHash(url);
        return path.join(this.cachePath, fileHash);
    }

    /**
     * Get the cache file, return null if the file does not exist
     * 
     * @param {String} url The url of the request
     * @returns string|bool
     */
    handleCacheFileForUrl(url) {
        const filePath = this.getCacheFilePathForUrl(url);
        if(!fs.existsSync(filePath)) {
            return null;
        }

        const cacheObject = JSON.parse(fs.readFileSync(filePath));
        const $ = cheerio.load(cacheObject.body);

        if(cacheObject.type == 'base') {
            this.parseBasePage($);
        } else {
            this.parseCurrencyPage($, cacheObject.iso, cacheObject.country);
        }

        return true;
    }
}