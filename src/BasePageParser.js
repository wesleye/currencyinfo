const utils = require('./utils');

module.exports = class BasePageParser {

    constructor(currencyCrawler) {
        this.foundCurrencies = [];
        this.urls = [];

        this.symbols = [];
        this.currencyCrawler = currencyCrawler;
    }

    /**
     * Parses the base page for currency url's and adds them
     * to the queue.
     * 
     * @param {Object} $ The crawled page
     */
    parseBasePage($) {
        // Find all rows
        var rows = $('.wikitable').find('tbody').find('tr');

        // Foreach Rows
        var previousCountry = null;
        rows.each((i, element) => {
            var row = $(element);

            // No td's in the row (header)? Skip it
            if(this.rowIsHeader(row)) {
                return;
            }

            const cells = row.find('td');
            const iso = this.parseCurrencyISO(cells);
            
            // Currencies not part of ISO 4217 are not listed
            if(iso === null) {
                return;
            }

            const currencyObject = this.getCurrencyObject(iso);

            /**
             * Countries with multiple currencies will have one row with 
             * 6 cells (the first row) and the following with 5 cells.
             * Consider 5 cells to belong to the previous found country.
             */

            // Single country
            var offset = 0;
            var country = null;
            if(cells.length == 6) {
                country = utils.trimText(cells.first().text());
                offset = 1;
            } else if(cells.length == 5) {
                country = previousCountry;
                offset = 0;
            } else {
                throw new Error('Could not parse row, unknown number of cells.')
            }

            // Associate this country to the currency
            if(currencyObject.countries.indexOf(previousCountry) === -1) {
                currencyObject.countries.push(previousCountry);
            }

            // Fill in the rest of the object
            currencyObject.name = utils.trimText(cells.eq(offset).text());
            currencyObject.symbols = utils.trimText(cells.eq(offset + 1).text()).split(' or ');

            this.symbols = this.symbols.concat(currencyObject.symbols);
            currencyObject.fractionalUnit = utils.trimText(cells.eq(offset + 3).text());
            currencyObject.numberToBasic = utils.trimText(cells.eq(offset + 4).text());
            
            const url = utils.parseUrl(cells.eq(offset).find('a').attr('href'));
            this.urls.push();

            setTimeout(() => {
                this.currencyCrawler.crawlPage(url, 'currency', iso, country);
            }, 500);
        });

        return {
            foundCurrencies: this.foundCurrencies,
            urls: this.urls
        }
    }

    /**
     * Parses one or multiple symbols, split by 'or' or comma.
     * 
     * @param {String} text Text of the symbol cell
     */
    parseSymbolData(text) {
        return utils.trimText(text).split(/,|or/).map(str => str.trim());
    }

    /**
     * Skip if the row contains no table data cells,
     * like an header row.
     *
     * @param {Object} row The tr row
     */
    rowIsHeader(row) {
        const cells = row.find('td');
        if(cells.length == 0) {
            return true;
        }

        return false;
    }

    /**
     * Get the ISO 4217 code for this currency.
     * 
     * The first row of a country with multiple currencies will have
     * 6 cells, where the ISO is contained in the 3th cell.
     * The following cells for that country will have 5 cells.
     * 
     * Countries with just one single currency will have 6 cells.
     * 
     * @param {Object} cells The cells of the row
     */
    parseCurrencyISO(cells) {
        let rawISO = null

        if(cells.length == 5) {
            rawISO = cells.eq(2).text();
        } else if(cells.length == 6) {
            rawISO = cells.eq(3).text();
        } else {
            throw new Error('Could not find ISO');
        }

        // Grab the first three letters
        var iso = rawISO.substr(0, 3);
        
        // No valid ISO, do not parse
        if(iso == "(no" || iso !== iso.toUpperCase()) {
            return null;
        }

        return iso;
    }

    getCurrencyObject(iso) {
        if(!(iso in this.foundCurrencies)) {
            this.foundCurrencies[iso] = this.createBaseCurrencyObject();
            this.foundCurrencies[iso].iso = iso;
        }

        return this.foundCurrencies[iso];
    }

    createBaseCurrencyObject() {
        return {
            'name': null,
            'symbols': [],
            'iso': null,
            'countries': [],
            'fractionalUnit': null,
            'numberToBasic': null,
            'banknotes': {
                'rarely': [],
                'frequently': []
            },
            'coins': {
                'rarely': [],
                'frequently': []
            }
        };
    }
}